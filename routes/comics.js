const express = require('express')
const mysql = require('mysql')

const db = require('../db')
const { isValidDate } = require('../utils')

const router = express.Router()

const sql = `
    SELECT id,
           DATE_FORMAT(post_date, '%Y-%m-%d') AS date,
           post_title                         AS title,
           post_content                       AS content,
           mouseover.meta_value               AS mouseover,
           (comment_status = 'open')          AS commentsOpen,
           comment_count                      AS commentCount
    FROM ff_posts

             INNER JOIN (
        SELECT object_id
        FROM ff_term_relationships
                 NATURAL JOIN ff_term_taxonomy
                 NATURAL JOIN ff_terms
        WHERE ff_terms.slug = 'de_de'
    ) AS category
                        ON (object_id = id)

             LEFT OUTER JOIN ff_postmeta AS mouseover
                        ON (mouseover.post_id = id)
                            AND (mouseover.meta_key = 'mouseover')

    WHERE (post_status = 'publish')
      AND (DATE_FORMAT(post_date, '%Y-%m-%d') = ?)
    LIMIT 1
`

router.get('/:date', (req, res) => {
  const { date } = req.params
  if (!isValidDate(date)) {
    return res.status(422).send({ error: 'Invalid date!' })
  }

  db.query(mysql.format(sql, [date]), (error, results, fields) => {
    if (error) throw error
    if (results.length < 1) {
      return res.status(404).send({ error: `No comic found for ${date}!` })
    }

    const [comic] = results
    comic.commentsOpen = comic.commentsOpen === 1

    const [year, month, day] = comic.date.split('-')
    comic.comicUrl = `https://fonflatter.de/${year}/${month}/${day}/`
    comic.imageUrl = `https://fonflatter.de/${year}/fred_${year}-${month}-${day}.png`

    res.send(comic)
  })
})

module.exports = router
