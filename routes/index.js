const express = require('express')

const router = express.Router()

router.use('/calendar', require('./calendar'))
router.use('/comics', require('./comics'))
router.use('/comments', require('./comments'))
router.use('/throwback-thursday', require('./throwback-thursday'))

module.exports = router
