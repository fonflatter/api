const express = require('express')

const db = require('./db')

const app = express()
const port = process.env.PORT || 3000

app.use(require('./routes'))

db.connect()
app.listen(port, () => {
  console.log(`Example app listening on port ${port}!`)
})

process.on('exit', () => {
  db.end()
})
