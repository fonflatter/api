const express = require('express')
const fs = require('fs')
const ICAL = require('ical.js')
const path = require('path')

const router = express.Router()

router.get('/:year', (req, res) => {
  const { year } = req.params
  if (year.length !== 4 || parseInt(year, 10).toString() !== year) {
    return res.status(422).send({ error: 'Invalid year!' })
  }

  const fileName = path.join(__dirname, '..', 'calendars', `${year}.ics`)
  const calendarFile = fs.readFileSync(fileName, 'utf-8')
  const calendar = ICAL.Component.fromString(calendarFile)
  const events = calendar
    .getAllSubcomponents('vevent')
    .map(vevent => new ICAL.Event(vevent))

  const eventMap = {}
  events.forEach(event => {
    const key = event.startDate.toString()
    if (!eventMap[key]) {
      eventMap[key] = []
    }

    eventMap[key].push(event.summary)
  })

  res.send(eventMap)
})

module.exports = router
