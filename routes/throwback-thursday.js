const express = require('express')
const mysql = require('mysql')

const db = require('../db')
const { isValidDate } = require('../utils')

const router = express.Router()

const sql = `
    SELECT DATE_FORMAT(post_date, '%Y-%m-%d') AS comicDate
    FROM ff_posts
             INNER JOIN ff_postmeta
                        ON (post_id = ID)
    WHERE (meta_value = ?)
    LIMIT 1
`

router.get('/:date', (req, res) => {
  const { date } = req.params
  if (!isValidDate(date)) {
    return res.status(422).send({ error: 'Invalid date!' })
  }

  db.query(mysql.format(sql, [date]), (error, results) => {
    if (error) throw error
    if (results.length < 1) {
      return res.status(404).send({ error: `No comic found for ${date}!` })
    }
    const { comicDate } = results[0]
    res.redirect(307, `../comics/${comicDate}`)
  })
})

module.exports = router
