module.exports = {
  isValidDate: dateString =>
    dateString.length === 10 && !Number.isNaN(Date.parse(dateString))
}
