const express = require('express')
const mysql = require('mysql')

const db = require('../db')

const router = express.Router()

const recentCommentsSql = `
    SELECT comment_ID      AS id,
           comment_author  AS authorName,
           comment_post_ID AS postId
    FROM ff_comments
    WHERE comment_approved = 1
    ORDER BY id DESC
    LIMIT 5
`

router.get('/recent', (req, res) => {
  db.query(recentCommentsSql, (error, comments) => {
    if (error) throw error

    res.send(comments)
  })
})

const postCommentsSql = `
    SELECT comment_ID         AS id,
           comment_author     AS authorName,
           comment_author_url AS authorUrl,
           comment_date_gmt   AS date,
           comment_content    AS text,
           user_id            AS userId
    FROM ff_comments
    WHERE comment_post_ID = ?
      AND comment_approved = 1
    ORDER BY id
`

router.get('/:postId', (req, res) => {
  const { postId } = req.params
  if (Number.isNaN(parseInt(postId, 10))) {
    return res.status(422).send({ error: 'Invalid post ID!' })
  }

  db.query(mysql.format(postCommentsSql, [postId]), (error, comments) => {
    if (error) throw error

    res.send(comments)
  })
})

module.exports = router
